$(document).on 'turbolinks:load', ->
  $('[data-toggle="tooltip"]').tooltip()
  $('select').formSelect()
  $('.material-icons.red-text').each ->
    if $(this).text() == 'check_box_outline_blank'
      $(this).addClass 'animated rubberBand infinite'
    return
  $('input, select').change ->
    $("button.send").trigger( "click" )
    return
  $('.collapsible').collapsible()
  return
