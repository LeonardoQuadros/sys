$(document).on 'turbolinks:load', ->
  elem = document.querySelector('#slide-modulos')
  instance = M.Sidenav.init(elem,
    onOpenStart: ->
      $('#slide-modulos').fadeIn 'slow'
      return
    onCloseStart: ->
      $('#slide-modulos').fadeOut()
      return
    onCloseEnd: ->
  )
  return