class Admin::ApplicationController < ActionController::Base
	include AdminHelper
	include UserHelper
  before_action :authenticate_user!
  before_action :garantir_autorizacao

	layout 'admin'


	rescue_from CanCan::AccessDenied do |exception|
		redirect_to '/dashboard', :alert => exception.message
	end
	
	private
	def garantir_autorizacao
    authorize! :acessar, 		:admin
	end

end
