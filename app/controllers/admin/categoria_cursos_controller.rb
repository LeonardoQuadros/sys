class Admin::CategoriaCursosController < Admin::ApplicationController
    before_action :set_categoria, only: [:show, :edit, :update, :destroy]

    def index
        conditions = []
        conditions << "categoria_cursos.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
        conditions << "categoria_cursos.nome LIKE '%#{params[:nome]}%'" unless params[:nome].blank?
        @categorias = CategoriaCurso.where(conditions.join(" AND ")).order("created_at desc")
    end

    def show
    end

    def edit
    end

    def update
        respond_to do |format|
            if @categoria.update(categoria_params)
                format.js {render inline: "M.toast({html: 'Atualizado!', classes: 'green'}); " }
            else
                format.js {render inline: "M.toast({html: 'Não foi possível editar a categoria #{@categoria.nome}', classes: 'red'}); " }
            end
        end
    end

    def new
    	@categoria = CategoriaCurso.new
    end

    def create
    	@categoria = CategoriaCurso.new(categoria_params)
    	if @categoria.save
            redirect_to edit_admin_categoria_curso_path(@categoria), notice: "Curso #{@categoria.nome} criado com sucesso!"
        else
            respond_to do |format|
                format.js
            end
        end
    end

    def destroy
    	@categoria.destroy unless @categoria.cursos.size > 0
       respond_to do |format|
          format.js
      end
  end

  private

  def categoria_params
      params.require(:categoria_curso).permit(:nome, :descricao, :ativo, {permitidos: []})
  end

  def set_categoria
   @categoria = CategoriaCurso.find(params[:id])
end
end
