class Admin::CursosController < Admin::ApplicationController
    before_action :set_curso, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

    def index
        conditions = []
        conditions << "cursos.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
        conditions << "cursos.nome LIKE '%#{params[:nome]}%'" unless params[:nome].blank?
        @cursos = Curso.where(conditions.join(" AND ")).order("created_at desc")
    end

    def show
    end

    def edit
    end

    def update
      if @curso.update(curso_params)
        if params[:curso][:imagem_capa].blank?
          respond_to do |format|
            format.js {render inline: "M.toast({html: 'Atualizado!', classes: 'green'}); " }
          end
        else
          redirect_to request.referrer, notice: "Atualizado"
        end
      else
        respond_to do |format|
          format.js
        end
      end
    end

    def new
    	@curso = Curso.new
    end

    def create
    	@curso = Curso.new(curso_params)
    	if @curso.save
            redirect_to edit_admin_curso_path(@curso), notice: "Curso #{@curso.nome} criado com sucesso!"
        else
            respond_to do |format|
                format.js
            end
        end
    end

    def destroy
      if @curso.modulos.size > 0
        Log.create(descricao: "Tentativa de deletar Curso!", objeto: {"id" => @curso.id, "tipo" => "cursos", "descricao" => "#{@curso.nome} - Tentativa de (#{current_user.id}) #{nome}", "class" => "orange", "user_id" => "#{current_user.id}"})
        respond_to do |format|
          format.js {render inline: "M.toast({html: 'Não foi possível deletar o curso #{@curso.nome}, módulos existentes.', classes: 'red'}); " }
        end
      else
        l = Log.new(descricao: "Curso deletado!", objeto: {"id" => @curso.id, "tipo" => "cursos", "descricao" => "#{@curso.nome} deletada por (#{current_user.id}) #{nome}", "class" => "red", "user_id" => "#{current_user.id}"})
        if @curso.destroy
          l.save
        else
          respond_to do |format|
            Log.create(descricao: "Tentativa de deletar Curso!", objeto: {"id" => @curso.id, "tipo" => "cursos", "descricao" => "#{@curso.nome} - Tentativa de (#{current_user.id}) #{nome}", "class" => "orange", "user_id" => "#{current_user.id}"})
            format.js {render inline: "M.toast({html: 'Não foi possível deletar o curso #{@curso.nome}, módulos existentes.', classes: 'red'}); " }
          end
        end 
      end
  end

  private

  def curso_params
      params.require(:curso).permit(:nome, :descricao, :ativo, {permitidos: []}, :gratis, :destaque, :vitalicio, :ementa, :imagem_capa, 
                                    :responsavel_id, :categoria_id, :preco, :custo, :desconto, :total)
  end

  def set_curso
   @curso = Curso.find(params[:id])
  end
end
