class Admin::ModulosController < Admin::ApplicationController
    before_action :set_modulo, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

    def index
      conditions = []
      conditions << "modulos.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
      conditions << "modulos.nome LIKE '%#{params[:nome]}%'" unless params[:nome].blank?
      unless params[:curso_id].blank?
        @curso = Curso.find(params[:curso_id]) 
        conditions << "modulos.curso_id = #{@curso.id}"
      end
      @modulos = Modulo.where(conditions.join(" AND ")).order("created_at desc")
    end

    def show
    end

    def edit
      @curso = @modulo.curso
    end

    def update
      if @modulo.update(modulo_params)
        if params[:modulo][:materiais].blank?
          respond_to do |format|
            format.js {render inline: "M.toast({html: 'Atualizado!', classes: 'green'}); " }
          end
        else
          redirect_to request.referrer, notice: "Atualizado!"
        end
      else
        respond_to do |format|
          format.js
        end
      end
    end

    def new
    	@modulo = Modulo.new
      unless params[:curso_id].blank?
        @curso = Curso.find(params[:curso_id])
        @modulo.curso_id = @curso.id
      end
    end

    def create
    	@modulo = Modulo.new(modulo_params)
    	if @modulo.save
            redirect_to edit_admin_modulo_path(id: @modulo.id, curso_id: @modulo.curso_id), notice: "Modulo #{@modulo.nome} criado com sucesso!"
        else
            respond_to do |format|
                format.js
            end
        end
    end

    def destroy
      l = Log.new(descricao: "Módulo deletado!", objeto: {"id" => @modulo.id, "tipo" => "modulos", "descricao" => "#{@modulo.nome} deletada por (#{current_user.id}) #{nome}", "class" => "red", "user_id" => "#{current_user.id}"})
    	if @modulo.destroy
        l.save
        redirect_to request.referrer, notice: "Módulo deletado!"
      else
        respond_to do |format|
          Log.new(descricao: "Tentativa de deletar Módulo!", objeto: {"id" => @modulo.id, "tipo" => "modulos", "descricao" => "#{@modulo.nome} - Tentativa de (#{current_user.id}) #{nome}", "class" => "orange", "user_id" => "#{current_user.id}"})
          format.js {render inline: "M.toast({html: 'Não foi possível deletar o módulo #{@modulo.nome}', classes: 'red'}); " }
        end
      end
  end

  private

  def modulo_params
      params.require(:modulo).permit(:nome, :descricao, :ativo, :curso_id, :conteudo, :tempo, :video, {materiais: []}, {curso_info: {}})
  end

  def set_modulo
   @modulo = Modulo.find(params[:id])
end
end
