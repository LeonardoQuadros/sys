class Admin::ProvasController < Admin::ApplicationController
    before_action :set_prova, only: [:show, :edit, :update, :destroy, :add_pergunta]
      skip_before_action :verify_authenticity_token

    def index
      @modulo = Modulo.find(params[:modulo_id])
      @provas = @modulo.provas.order("created_at desc")
    end

    def show
    end

    def edit
      @modulo = Modulo.find(params[:modulo_id])
    end

    def update
        respond_to do |format|
            if @prova.update(prova_params)
                format.js {render inline: "M.toast({html: 'Atualizada!', classes: 'green'}); " }
            else
                format.js
            end
        end
    end

    def new
    	@prova = Prova.new
      @modulo = Modulo.find(params[:modulo_id])
      @prova.modulo_id = @modulo.id
      @prova.responsavel_id = current_user.id
    end

    def create
    	@prova = Prova.new(prova_params)
    	if @prova.save
            redirect_to edit_admin_prova_path(id: @prova.id, modulo_id: @prova.modulo_id), notice: "Prova #{@prova.descricao} criada com sucesso!"
        else
            respond_to do |format|
                format.js
            end
        end
    end

    def destroy
      l = Log.new(descricao: "Prova deletada!", objeto: {"id" => @prova.id, "tipo" => "provas", "descricao" => "#{@prova.descricao} deletada por (#{current_user.id}) #{nome}", "class" => "red", "user_id" => "#{current_user.id}"})
      if @prova.destroy
        l.save
        redirect_to request.referrer, notice: "Prova deletada!"
      else
        respond_to do |format|
          Log.new(descricao: "Tentativa de deletar Prova!", objeto: {"id" => @prova.id, "tipo" => "provas", "descricao" => "#{@prova.descricao} - Tentativa de (#{current_user.id}) #{nome}", "class" => "orange", "user_id" => "#{current_user.id}"})
          format.js {render inline: "M.toast({html: 'Não foi possível deletar o módulo #{@prova.descricao}', classes: 'red'}); " }
        end
      end
  end

  private

  def prova_params
      params.require(:prova).permit(:descricao, :ativo, :modulo_id, :tempo, :responsavel_id, {perguntas: {}},
                    perguntas_attributes: [:id, :frase, :tipos, :_destroy, 
                                          respostas_attributes: [:id, :frase, :correta, :_destroy]])
  end

  def set_prova
   @prova = Prova.find(params[:id])
end
end
