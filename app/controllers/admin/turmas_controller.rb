class Admin::TurmasController < Admin::ApplicationController
    before_action :set_turma, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

    def index
      conditions = []
      conditions << "turmas.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
      conditions << "turmas.descricao LIKE '%#{params[:descricao]}%'" unless params[:descricao].blank?
      unless params[:curso_id].blank?
        @curso = Curso.find(params[:curso_id]) 
        conditions << "turmas.curso_id = #{@curso.id}"
      end
      @turmas = Turma.where(conditions.join(" AND ")).order("created_at desc")
    end

    def show
    end

    def edit
      @curso = @turma.curso
    end

    def update
      if @turma.update(turma_params)
        if params[:turma][:materiais].blank?
          respond_to do |format|
            format.js {render inline: "M.toast({html: 'Atualizado!', classes: 'green'}); " }
          end
        else
          redirect_to request.referrer, notice: "Atualizado!"
        end
      else
        respond_to do |format|
          format.js
        end
      end
    end

    def new
    	@turma = Turma.new
      unless params[:curso_id].blank?
        @curso = Curso.find(params[:curso_id])
        @turma.curso_id = @curso.id
      end
    end

    def create
    	@turma = Turma.new(turma_params)
      if @turma.save
        @turma.update(descricao: "#{@turma.curso.nome} - Turma #{@turma.inicio.to_date.month}/#{@turma.inicio.to_date.year}")
            redirect_to edit_admin_turma_path(id: @turma.id, curso_id: @turma.curso_id), notice: "Turma #{@turma.descricao} criado com sucesso!"
        else
            respond_to do |format|
                format.js
            end
        end
    end

    def destroy
      l = Log.new(descricao: "Turma deletada!", objeto: {"id" => @turma.id, "tipo" => "turmas", "descricao" => "#{@turma.descricao} deletada por (#{current_user.id}) #{nome}", "class" => "red", "user_id" => "#{current_user.id}"})
      if @turma.destroy
        l.save
        redirect_to request.referrer, notice: "Turma deletada!"
      else
        respond_to do |format|
          Log.new(descricao: "Tentativa de deletar Turma!", objeto: {"id" => @turma.id, "tipo" => "turmas", "descricao" => "#{@turma.descricao} - Tentativa de (#{current_user.id}) #{nome}", "class" => "orange", "user_id" => "#{current_user.id}"})
          format.js {render inline: "M.toast({html: 'Não foi possível deletar a turma #{@turma.descricao}', classes: 'red'}); " }
        end
      end
  end

  private

  def turma_params
      params.require(:turma).permit(:descricao, :ativo, :curso_id, :inicio, :fim, :inicio_inscricao, :fim_inscricao, :livros, :observacoes,
                                    :limite_inscritos, :professor_id, :ementa)
  end

  def set_turma
   @turma = Turma.find(params[:id])
end
end
