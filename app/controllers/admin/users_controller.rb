class Admin::UsersController < Admin::ApplicationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

    def index
      conditions = []
      conditions << "users.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
      conditions << "users.nomes_fisicos ->> lower('NOME') LIKE '%#{params[:nome] }%' " unless params[:nome].blank?
      @users = User.where(conditions.join(" AND ")).order("created_at desc")
    end

    def show
    end

    def edit
    end

    def update
      if @user.update(user_params)
        if params[:user][:fotos].blank?
          respond_to do |format|
            format.js {render inline: "M.toast({html: 'Atualizado!', classes: 'green'}); " }
          end
        else
          redirect_to request.referrer, notice: "Atualizado!"
        end
      else
        respond_to do |format|
          format.js
        end
      end
    end

    def new
    	@user = User.new
    end

    def create
    	@user = User.new(user_params)
      if @user.save
            redirect_to edit_admin_user_path(id: @user.id, curso_id: @user.curso_id), notice: "User #{@user.nome} criado com sucesso!"
        else
            respond_to do |format|
                format.js
            end
        end
    end

    def destroy
      l = Log.new(descricao: "Usuário deletado!", objeto: {"id" => @user.id, "tipo" => "users", "descricao" => "#{@user.nome} deletado por (#{current_user.id}) #{nome}", "class" => "red", "user_id" => "#{current_user.id}"})
      if @user.destroy
        l.save
        redirect_to request.referrer, notice: "Usuário deletado!"
      else
        respond_to do |format|
          Log.new(descricao: "Tentativa de deletar Usuário!", objeto: {"id" => @user.id, "tipo" => "users", "descricao" => "#{@user.nome} - Tentativa de (#{current_user.id}) #{nome}", "class" => "orange", "user_id" => "#{current_user.id}"})
          format.js {render inline: "M.toast({html: 'Não foi possível deletar a user #{@user.nome}', classes: 'red'}); " }
        end
      end
  end

  private

  def user_params
      params.require(:user).permit(:nome, :email, :password, :celular, :telefone, :cpf, :rg, :data_nascimento, {fotos: []})
  end

  def set_user
   @user = User.find(params[:id])
end
end
