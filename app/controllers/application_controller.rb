class ApplicationController < ActionController::Base
	layout 'frontend'

	rescue_from CanCan::AccessDenied do |exception|
		redirect_to '/', :alert => exception.message
	end
	
end
