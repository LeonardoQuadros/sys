class Dashboard::CursosController < Dashboard::ApplicationController

  def index
    conditions = []
    conditions << "cursos.categoria_id IN (#{params[:categoria_id].split(/\W+/).uniq.join(', ')})" unless params[:categoria_id].blank?
    conditions << "cursos.ativo = #{true}"
    @cursos = Curso.where(conditions.join(' AND ')).order("created_at desc")
    authorize! :read,    Curso
  end

  def show
    @curso = Curso.find(params[:id])
  end

  def gratis
  end

  def adquirir
  	@curso = Curso.find(params[:id])
    if current_user.adquirir_curso!(@curso)
      redirect_to request.referrer, notice: 'Curso Adquirido!'
    else
      redirect_to request.referrer, alert: "Não foi possível Adquirir o curso #{@curso.nome}"
    end
  end

end
