class Dashboard::ModulosController < Dashboard::ApplicationController

  def show
    @modulo = Modulo.find(params[:id])
    @curso = @modulo.curso
  end
end
