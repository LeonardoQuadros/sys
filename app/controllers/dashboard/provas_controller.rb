class Dashboard::ProvasController < Dashboard::ApplicationController

  def show
    @prova = Prova.find(params[:id])
    @modulo = @prova.modulo
    @curso = @prova.modulo.curso
  end
end
