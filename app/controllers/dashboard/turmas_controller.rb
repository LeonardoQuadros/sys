class Dashboard::TurmasController < Dashboard::ApplicationController
  before_action :set_turma, only: [:show, :inscrever_aluno]


  def show
    @curso = @turma.curso
  end

  def inscrever_aluno
    m = Matricula.new(aluno_id: current_user.id, turma_id: @turma.id)
    if @turma.fim_inscricao.to_date < Date.today
      respond_to do |format|
          format.js {render inline: "M.toast({html: 'Tempo para inscrição terminou.', classes: 'red'}); " }
      end
    elsif m.save
  		redirect_to dashboard_curso_path(@turma.curso), notice: "Inscrito - #{@turma.descricao}"
  	else
      respond_to do |format|
          format.js {render inline: "M.toast({html: '#{m.errors.full_messages.first}', classes: 'red'}); " }
      end
  	end
  end

  private

  def set_turma
   @turma = Turma.find(params[:id])
  end

end
