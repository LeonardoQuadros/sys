class Dashboard::UsersController < Dashboard::ApplicationController

  def update
    if current_user.update(user_params)
      if params[:user][:fotos].blank?
      respond_to do |format|
        format.js {render inline: "M.toast({html: 'Atualizado!', classes: 'green'}); " }
      end
      else
        redirect_to request.referrer, notice: "Atualizado"
      end
    else
      respond_to do |format|
        format.js {render inline: "M.toast({html: '#{current_user.errors.messages}', classes: 'red'}); " }
      end
    end
  end

  private

  def user_params
      params.require(:user).permit(:nome, :email, :password, :celular, :telefone, :cpf, :rg, :data_nascimento, {fotos: []})
  end

end
