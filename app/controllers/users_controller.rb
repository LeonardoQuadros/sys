class UsersController < ApplicationController

	def create
    @user = User.new(user_params)
    @user.save
	end

	private

    def user_params
    	if Rails.env.test? or Rails.env.development?
    		{nome: params[:user][:nome], sobrenome: params[:user][:sobrenome], email: params[:user][:email], password: params[:user][:password] }
    	else
      	params.require(:user).permit(:email, :nome, :password)
    	end
    end
end
