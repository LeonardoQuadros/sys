module ApplicationHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

	def foto_perfil
		if current_user.blank? or current_user.documentos["PERFIL"].blank?
			"/images/avatars/#{rand(1..12)}.png"
		else
			current_user.documentos["PERFIL"]
		end
	end

  def log_unviewed
    Log.where("logs.created_at BETWEEN '#{(DateTime.now-(1.day)).beginning_of_day}' AND '#{DateTime.now.end_of_day}'")
  end

  def is_new?objeto
    objeto.created_at > (DateTime.now-(1.day)).beginning_of_day
  end

  def calcular_tempo tempo

    if tempo.to_f > 1512
      return "#{(tempo.to_f/720).to_i} meses"
    elsif tempo.to_f > 100
      return "#{(tempo.to_f/24).to_i} dias"
    elsif tempo.to_f > 2
      return "#{(tempo).to_i} horas"
    else
      return "#{((tempo).to_i*60).to_i} minutos"
    end
  end
  
end
