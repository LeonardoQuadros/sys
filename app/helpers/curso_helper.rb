module CursoHelper
	def capa curso
		if curso.imagem_capa.blank?
			return "/images/tecnologia/#{rand(1..17)}.jpg"
		else
			return curso.imagem_capa.fill_medium
		end
	end

	def curso_novo? curso
		curso.created_at > DateTime.now-1.day
	end

	def custo curso
		curso.valores["CUSTO"].to_f
	end

	def preco curso
		if curso.valores["PRECO"].to_f.zero?
			return ""
		else
			return "R$ #{curso.valores["PRECO"].to_s.gsub('.','').to_f}"
		end
	end

	def total curso
		curso.valores["TOTAL"].to_f
	end

	def string_total curso
		if curso.valores["TOTAL"].to_f.zero?
			return "Grátis"
		else
			return "R$ #{curso.valores["TOTAL"].to_f}"
		end
	end

	def desconto curso
		curso.valores["DESCONTO"]
	end

	def total curso
		curso.valores["TOTAL"]
	end

	def form_id curso
		if curso.new_record?
			"new_curso"
		else
			"edit_curso_#{curso.id}"
		end
	end

	def cursos_gratis
		Curso.where(gratis: true, ativo: true)
	end

	def curso_gratis? curso
		curso.gratis
	end

	def curso_adquirido? curso
		current_user.cursos.keys.include?"#{curso.id}"
	end

	def cursos_de_turmas
		Curso.ativos.where(vitalicio: false)
	end

	def curso_vitalicio? curso
		curso.vitalicio
	end

end
