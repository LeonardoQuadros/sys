module ModuloHelper

	def modulo_atual curso
		mds = current_user.modulos_iniciados.where(modulo_id: curso.modulos.ids)
		if mds.blank?
			return false
		else
			return Modulo.where(id: mds.map {|mod| mod.modulo_id }).ordem("ordem asc").last
		end
	end
end
