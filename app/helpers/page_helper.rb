module PageHelper

  def descricao_log log
    "#{log.objeto['descricao']}"
  end    
  
  def log_url log
    "/admin/#{log.objeto['tipo']}/#{log.objeto['id']}/"
  end    
  
  def new_log log
     [Date.today, Date.yesterday].include?(log.created_at.to_date)
  end    

  def cursos
  	Curso.ativos
  end

  def is_home?(controller, curso)
    if action_name == 'home'
      return true
    elsif !curso.blank? and current_user.cursos.keys.include?curso.id.to_s
      return true
    else
      return false
    end
  end

  def meus_cursos
    Curso.where(id: current_user.cursos.keys)
  end
end
