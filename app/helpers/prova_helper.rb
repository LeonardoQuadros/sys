module ProvaHelper

	def pergunta_tipo
    Pergunta.tipos.map do |r, key|
     [I18n.t("pergunta.tipos.#{r}"), r]
    end
	end

end
