module TurmaHelper

	def turma_tempo_inscricao turma
		tempo = (turma.fim_inscricao.to_date-Date.today)*24
    if tempo.to_f > 1512
      return "#{(tempo.to_f/720).to_i} meses"
    elsif tempo.to_f > 100
      return "#{(tempo.to_f/24).to_i} dias"
    elsif tempo.to_f > 2
      return "#{(tempo).to_i} horas"
    else
      return "#{((tempo).to_i*60).to_i} minutos"
    end
	end

  def turma_inicio turma
    l turma.inicio.to_date, format: :short
  end

  def turma_fim turma
    l turma.fim.to_date, format: :short
  end

  def turma_inscritos turma
    turma.matriculas
  end

  def turma_inscrito? (turma, user = current_user)
    user.turmas.ids.include? turma.id
  end

end
