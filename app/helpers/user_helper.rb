module UserHelper

	def user_cursos user
		Curso.where(id: user.cursos.keys)
	end

	def admin(user = current_user)
		(user.tipos["ADMIN"] == true or user.tipos["SUPERADMIN"] == true )
	end

	def nome(user = current_user)
		"#{user.nomes_fisicos["NOME"]}" 
	end
	def sobrenome(user = current_user)
		"#{user.nomes_fisicos["SOBRENOME"]}" 
	end
	def email(user = current_user)
		current_user.email
	end
	def nome_completo(user = current_user)
		"#{user.nomes_fisicos["NOME"]} #{user.nomes_fisicos["SOBRENOME"]}" 
	end
	def nome_fantasia(user = current_user)
		"#{user.dados_juridicos["NOME_FANTASIA"]}" 
	end
	def razao_social(user = current_user)
		"#{user.dados_juridicos["RAZAO_SOCIAL"]}" 
	end
	def celular(user = current_user)
		"#{user.fones["CELULAR"]}" 
	end
	def telefone(user = current_user)
		"#{user.fones["TELEFONE"]}" 
	end
	def data_nascimento(user = current_user)
		"#{user.datas["DATA_NASCIMENTO"]}" 
	end
	def renda(user = current_user)
		"#{user.rendas["RENDA"]}" 
	end
	def renda_familiar(user = current_user)
		"#{user.rendas["RENDA_FAMILIAR"]}" 
	end
	def tipos(user = current_user)
		if user.tipos["SUPERADMIN"] == true
			return "SUPERADMIN"
		elsif user.tipos["ADMIN"] == true
			return "ADMIN"
		elsif user.tipos["PROFESSOR"] == true
			return "PROFESSOR"
		elsif user.tipos["ALUNO"] == true
			return "ALUNO"
		end
	end
	def cpf(user = current_user)
		"#{user.documentos["CPF"]}" 
	end
	def cnpj(user = current_user)
		"#{user.documentos["CNPJ"]}" 
	end
	def rg(user = current_user)
		"#{user.documentos["RG"]}" 
	end
	def creci(user = current_user)
		"#{user.documentos["CRECI"]}" 
	end
	def crea(user = current_user)
		"#{user.documentos["CREA"]}" 
	end
	def inscricao_estadual(user = current_user)
		"#{user.documentos["INSCRICAO_ESTADUAL"]}" 
	end
	def inscricao_municipal(user = current_user)
		"#{user.documentos["INSCRICAO_MUNICIPAL"]}" 
	end
	def pais(user = current_user)
		"#{user.enderecos["PAIS"]}" 
	end
	def estado(user = current_user)
		"#{user.enderecos["ESTADO"]}" 
	end
	def cidade(user = current_user)
		"#{user.enderecos["CIDADE"]}" 
	end
	def bairro(user = current_user)
		"#{user.enderecos["BAIRRO"]}" 
	end
	def rua(user = current_user)
		"#{user.enderecos["RUA"]}" 
	end
	def numero(user = current_user)
		"#{user.enderecos["NUMERO"]}" 
	end
	def complemento(user = current_user)
		"#{user.enderecos["COMPLEMENTO"]}" 
	end
	def cep(user = current_user)
		"#{user.enderecos["CEP"]}" 
	end
end
