class Ability
    include CanCan::Ability

    def initialize(user)
        user ||= User.new # guest user (not logged in)
        
        if user.tipos["SUPERADMIN"] == true
            can :manage, :all
        else
            can :manage,    Page
            can [:adquirir], Curso, Curso.all do |curso|
                curso.ativo
            end
            can [:read, :gratis], Curso, Curso.all do |curso|
                curso.ativo or user.cursos.keys.include?"#{curso.id}"
            end

            can [:read], Modulo, Modulo.all do |modulo|
                user.cursos.keys.include?"#{modulo.curso.id}"
            end
        end
    end
end
