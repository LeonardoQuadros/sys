class CategoriaCurso < ApplicationRecord

  validates :nome, presence: true
	validates :nome, uniqueness: true

  has_many :cursos, foreign_key: "categoria_id", dependent: :destroy
  
end