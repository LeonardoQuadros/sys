class Curso < ApplicationRecord

  validates :nome, :categoria, :responsavel, presence: true
  validates :nome, uniqueness: true

  validate :ter_modulos

  belongs_to :categoria, :class_name => "CategoriaCurso"
  belongs_to :responsavel, :class_name => "User"
  has_many :modulos, dependent: :destroy
  has_many :turmas, dependent: :destroy

  mount_uploader :imagem_capa, ImagemUploader

  scope :ativos, -> {where("cursos.ativo = #{true}")}

  attr_accessor :custo, :preco, :desconto, :total

  after_create :log_for_create
  #after_update :log_for_update, if: -> { self.changes }

  def initialize(attributes = {})

    unless attributes.blank? 
      total = attributes[:total]
      attributes.merge!(:historico                                            => {"#{DateTime.now}" => Hash[attributes]}  )
      attributes.merge!(:valores                                              => {'CUSTO' => attributes[:custo], 'PRECO' => attributes[:preco], 'DESCONTO' => attributes[:desconto], 'TOTAL' => attributes[:total]}  )
      if total.to_f > 0
        attributes.merge!(:gratis                                            => false)
      else
        attributes.merge!(:gratis                                            => true)
      end
    end
    super
  end

  def update(attributes = {})

    total = attributes["total"] || self.valores["TOTAL"]
    attributes.merge!(ativo: "#{(["1", "true"].include?attributes['ativo'].to_s ) ? true : false}") unless attributes['ativo'].blank?
    attributes.merge!(destaque: "#{(["1", "true"].include?attributes['destaque'].to_s ) ? true : false}") unless attributes['destaque'].blank?
    attributes.merge!(:valores                                              => {'CUSTO' => attributes["custo"], 'PRECO' => attributes["preco"], 'DESCONTO' => attributes["desconto"], 'TOTAL' => attributes["total"]}  ) if attributes[:valores].blank? and !attributes["preco"].blank?
    attributes.select { |x| attributes.delete(x) if ["custo", "preco", "desconto", "total"].include?(x) } 
    historico = attributes.select { |key, value| {key => value} if "#{value}" != "#{self.attributes.with_indifferent_access[key]}" }.except("imagem_capa", "updated_at")
    attributes.merge!(:historico                                            => {"#{DateTime.now}" => historico}.merge(self.historico)   )
    if total.to_f > 0
      attributes.merge!(gratis: false)
    else
      attributes.merge!(gratis: true)
    end
    super    
  end

  def ter_modulos
    if self.modulos.blank? and self.ativo 
      errors.add(:ops, '~> Para poder ativar este curso, crie um Módulo.')
      return false
    end
    return true
  end

  def log_for_create
    Log.create(descricao: "Novo curso criado!", objeto: {"class" => "green", "id" => self.id, "tipo" => "cursos", "descricao" => self.nome})
  end

  def log_for_update
    Log.create(descricao: "Alteração no curso #{self.id} - #{self.nome}", objeto: {"class" => "green", "id" => self.id, "tipo" => "cursos", "descricao" => self.nome}) 
  end
  
end