class Matricula < ApplicationRecord

  validates :turma, :aluno, presence: true
  validates_uniqueness_of :aluno, :scope => :turma, :message=> "Já está inscrito nesta turma."


  belongs_to :turma
  belongs_to :aluno, :class_name => "User"

  after_create :log_for_create
  #after_update :log_for_update, unless: -> { self.changes.blank? }

  def log_for_create
    Log.create(descricao: "Nova matrícula criada!", objeto: {"class" => "green", "id" => self.id, "tipo" => "matriculas", "descricao" => "#{self.aluno.nome} - matrícula na turma #{self.turma.descricao}"})
  end

  def log_for_update
    Log.create(descricao: "Alteração na matrícula #{self.id} - #{self.descricao}", objeto: {"id" => self.id, "tipo" => "matriculas", "descricao" => "#{self.aluno.nome} - matrícula na turma #{self.turma.descricao}"}) 
  end

end