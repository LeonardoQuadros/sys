class Modulo < ApplicationRecord

  validates :nome, :curso, presence: true
  validates :nome, uniqueness: { scope: :curso }

  belongs_to :curso
  has_many :provas

  mount_uploaders :materiais, DocumentoUploader
  skip_callback :commit, :after, :mark_remove_materiais_false
  skip_callback :commit, :after, :remove_previously_stored_materiais

  scope :ativos, -> {where("modulos.ativo = #{true}")}

  after_create :log_for_create
  #after_update :log_for_update, unless: -> { self.changes.blank? }

  def log_for_create
    Log.create(descricao: "Novo modulo criado!", objeto: {"class" => "green", "id" => self.id, "tipo" => "modulos", "descricao" => self.nome})
  end

  def log_for_update
    Log.create(descricao: "Alteração no modulo #{self.id} - #{self.nome}", objeto: {"class" => "green", "id" => self.id, "tipo" => "modulos", "descricao" => self.nome}) 
  end

end