class ModulosIniciado < ApplicationRecord

  validates :user, :modulo, presence: true
  validates :modulo, uniqueness: { scope: :user }

  belongs_to :user
  belongs_to :modulo

  after_create :log_for_create
  #after_update :log_for_update, unless: -> { self.changes.blank? }

  def log_for_create
    modulo = self.modulo
    Log.create(descricao: "Novo modulo iniciado!", objeto: {"id" => self.id, "tipo" => "modulos_iniciados", "descricao" => "#{modulo.nome}"})
    modulo.feedback["iniciaods"] += 1
    modulo.save
  end

  def log_for_update
    Log.create(descricao: "Alteração no modulo #{self.id} - #{self.nome}", objeto: {"id" => self.id, "tipo" => "modulos", "descricao" => self.nome})
  end

end