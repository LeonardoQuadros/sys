class Pergunta < ApplicationRecord

  enum tipos: [:alternativa, :dissertativa]

  validates :frase, :prova, presence: true

  belongs_to :prova
  has_many   :respostas, dependent: :destroy
  accepts_nested_attributes_for :respostas,  allow_destroy: true

  
end