class Prova < ApplicationRecord

  validates :descricao, :modulo, :responsavel, presence: true
  #validates :modulo, uniqueness: true

  validate :ter_perguntas

  belongs_to    :modulo
  belongs_to    :responsavel, :class_name => "User"
  has_many      :perguntas, dependent: :destroy
  accepts_nested_attributes_for :perguntas,  allow_destroy: true
  
  
  scope :ativos, -> {where("provas.ativo = #{true}")}

  after_create :log_for_create
  #after_update :log_for_update, unless: -> { self.changes.blank? }




  def ter_perguntas
    if self.perguntas.blank? and self.ativo 
      errors.add(:ops, '~> Para poder ativar esta prova, crie uma Pergunta.')
      return false
    end
    return true
  end

  def log_for_create
    Log.create(descricao: "Nova prova criada!", objeto: {"class" => "green", "id" => self.id, "tipo" => "provas", "descricao" => self.descricao})
  end

  def log_for_update
    Log.create(descricao: "Alteração na prova #{self.id} - #{self.descricao}", objeto: {"class" => "green", "id" => self.id, "tipo" => "provas", "descricao" => self.descricao}) 
  end

end