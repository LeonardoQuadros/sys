class Resposta < ApplicationRecord

  validates :frase, :pergunta, presence: true

  belongs_to :pergunta

end