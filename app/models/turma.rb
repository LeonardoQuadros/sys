class Turma < ApplicationRecord

  validates :curso, :inicio, :fim, :inicio_inscricao, :fim_inscricao, presence: true

  belongs_to :curso
  belongs_to :professor, :class_name => "User"
  has_many :matriculas

  scope :ativos, -> {where("turmas.ativo = #{true}")}

  after_create :log_for_create
  #after_update :log_for_update, unless: -> { self.changes.blank? }

  def log_for_create
    Log.create(descricao: "Nova turma criada!", objeto: {"class" => "green", "id" => self.id, "tipo" => "turmas", "descricao" => "#{self.curso.nome} - Turma #{self.inicio.to_date.month}/#{self.inicio.to_date.year}"})
  end

  def log_for_update
    Log.create(descricao: "Alteração na turma #{self.id} - #{self.descricao}", objeto: {"class" => "green", "id" => self.id, "tipo" => "turmas", "descricao" => "#{self.curso.nome} - Turma #{self.inicio.to_date.month}/#{self.inicio.to_date.year}"}) 
  end

end