class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :validatable

  validate :obrigatoriedades

  has_many :modulos_iniciados, dependent: :destroy
  has_many :matriculas, foreign_key: "aluno_id"
  has_many :turmas, through: :matriculas

  mount_uploaders :fotos, ImagemUploader

  after_create :log_for_create
  #after_update :log_for_update, if: -> { self.changes }

  attr_accessor :nome, :celular, :telefone, :data_nascimento, :cpf, :rg

  #override constructor
  def initialize(attributes = {})
    unless attributes.blank?
      attributes.merge!(:nomes_fisicos                                              => {'NOME' => attributes[:nome], 'SOBRENOME' => attributes[:sobrenome]}  )
      attributes.merge!(:dados_juridicos                                            => {'FANTASIA' => attributes[:nome_fantasia], 'NOME' => attributes[:razao_social], 'CNPJ' => attributes[:cnpj]}  )
      attributes.merge!(:enderecos                                                  => {'PAIS' => attributes[:pais], 'ESTADO' => attributes[:estado], 'CIDADE' => attributes[:cidade], 'BAIRRO' => attributes[:bairro], 'RUA' => attributes[:rua], 'NUMERO' => attributes[:numero], 'COMPLEMENTO' => attributes[:complemento], 'CEP' => attributes[:cep] } )
      attributes.merge!(:fones                                                      => {'CELULAR' => attributes[:celular], 'TELEFONE' => attributes[:telefone]})
      attributes.merge!(:datas                                                      => {'DATA_NASCIMENTO' => attributes[:data_nascimento]})
      attributes.merge!(:rendas                                                     => {'RENDA' => attributes[:renda], 'RENDA_FAMILIAR' => attributes[:renda_familiar]})
      attributes.merge!(:documentos                                                 => {'CPF' => attributes[:cpf], 'CNPJ' => attributes[:cnpj], 'RG' => attributes[:rg], 'CRECI' => attributes[:creci], 'CREA' => attributes[:crea], 'INSCRICAO_ESTADUAL' => attributes[:inscricao_estadual], 'INSCRICAO_MUNICIPAL' => attributes[:inscricao_municipal] })
      attributes.merge!(:origens                                                    => {'NACIONALIDADE' => attributes[:nacionalidade], 'NATURALIDADE' => attributes[:naturalidade]})
      attributes.merge!(:tipos                                                      => {'SUPERADMIN' => attributes[:superadmin], 'ADMIN' => attributes[:admin], 'PROFESSOR' => attributes[:professor], 'ALUNO' => attributes[:aluno] })
      attributes.merge!(:conjuges                                                   => {'ESTADO_CIVIL' => attributes[:estado_civil], 'NOME' => attributes[:conjuge_nome], 'EMAIL' => attributes[:conjuge_email], 'FONE' => attributes[:conjuge_fone], 'CPF' => attributes[:conjuge_cpf], 'RG' => attributes[:conjuge_rg]})
      ((attributes.keys)-(User.column_names.map { |x| x.to_sym })).map { |x| attributes.delete(x.to_sym) unless x.to_s == "password" }
    end
    super
  end

  #override update
  def update(attributes = {})
    unless attributes.blank?
      attributes.merge!(:nomes_fisicos                                              => self.nomes_fisicos.merge({'NOME' => attributes['nome']}))
      attributes.merge!(:fones                                                      => self.fones.merge({'CELULAR' => attributes['celular'], 'TELEFONE' => attributes['telefone']}))
      attributes.merge!(:datas                                                      => self.datas.merge({'DATA_NASCIMENTO' => attributes['data_nascimento']}))
      attributes.merge!(:documentos                                                 => self.documentos.merge({'CPF' => attributes['cpf'], 'RG' => attributes['rg']}))
      attributes.select { |x| attributes.delete(x) if ["nome", "celular", "telefone", "data_nascimento", "cpf", "rg"].include?(x) } 
    end
    super
  end

  def superadmin!
    self.tipos["SUPERADMIN"] = true
    self.save
  end

  def admin!
    self.tipos["ADMIN"] = true
    self.save
  end

  def professor!
    self.tipos["PROFESSOR"] = true
    self.save
  end

  def adquirir_curso! curso
    self.cursos["#{curso.id}"] = {"adquirido_em" => DateTime.now, "nome" => curso.nome, "descricao" => curso.descricao, "categoria" => curso.categoria.nome, "imagem_capa" => curso.imagem_capa.url, "modulos_ids" => curso.modulos.ids}
    self.save
  end

  def nome
    self.nomes_fisicos["NOME"]
  end

  def obrigatoriedades
    if self.nomes_fisicos["NOME"].blank?
      errors.add(:erro, '~> Preencha o campo NOME.')
      return false
    end
    return true
  end

  def log_for_create
    Log.create(descricao: "Novo usuário criado!", objeto: {"class" => "green", "id" => self.id, "tipo" => "users", "descricao" => self.nomes_fisicos["NOME"]})
  end

  def log_for_update
    Log.create(descricao: "Alteração no usuário #{self.id} - #{self.nomes_fisicos['NOME']}", objeto: {"id" => self.id, "tipo" => "user", "descricao" => self.nomes_fisicos['NOME']}) 
  end

end

=begin
  	add_column 						:users, :nomes_fisicos, :json, default: {} #nome, sobrenome
  	add_column 						:users, :dados_juridicos, :json, default: {} #nome_fantasia, razão_social
  	add_column 						:users, :enderecos, :json, default: {} #rua, bairro, cidade, estado etc
  	add_column 						:users, :conjuges, :json, default: {} #nome, sobrenome, cpf, rg, estado_civil
  	add_column 						:users, :fones, :json, default: {} #celular, telefone, fax etc
    add_column            :users, :datas, :json, default: {} #celular, telefone, fax etc
  	add_column 						:users, :documentos, :json, default: {} #rg, cpf, cnpj, creci, crea etc
  	add_column 						:users, :rendas, :json, default: {} #renda, renda_familiar 
  	add_column 						:users, :origens, :json, default: {} #naturalidade, nacionalidade
  	add_column 						:users, :profissoes, :json, default: {} #estudante, empresário, engenheiro, professor etc
  	add_column 						:users, :fotos, :json, default: {} #perfil, capa etc
  	add_column 						:users, :permissoes, :json, default: {} #usuario_visualiar, usuario_editar, usuario_'action' ... etc
  	add_column 						:users, :historicos_ips, :json, default: {} # self.historicos_ips["IP"] = {}
    add_column            :users, :tipos, :json, default: {"CORRETOR"=>false, "PROPRIETARIO"=>false, "ADMIN"=>false, "SUPERADMIN"=>false} # corretor, proprietário, admin, proponente etc

    @nomes_fisicos                                              = {'NOME' => attributes[:nome]} unless attributes[:nome].blank?
    @nomes_fisicos                                              = {'SOBRENOME' => attributes[:sobrenome]} unless attributes[:sobrenome].blank?
    @dados_juridicos                                            = {'NOME_FANTASIA' => attributes[:nome_fantasia]} unless attributes[:nome_fantasia].blank?
    @dados_juridicos                                            = {'RAZAO_SOCIAL' => attributes[:razao_social]} unless attributes[:razao_social].blank?
    @enderecos                                                  = {'PAIS' => attributes[:pais]} unless attributes[:pais].blank?
    @enderecos                                                  = {'ESTADO' => attributes[:estado]} unless attributes[:estado].blank?
    @enderecos                                                  = {'CIDADE' => attributes[:cidade]} unless attributes[:cidade].blank?
    @enderecos                                                  = {'BAIRRO' => attributes[:bairro]} unless attributes[:bairro].blank?
    @enderecos                                                  = {'RUA' => attributes[:rua]} unless attributes[:rua].blank?
    @enderecos                                                  = {'NUMERO' => attributes[:numero]} unless attributes[:numero].blank?
    @enderecos                                                  = {'COMPLEMENTO' => attributes[:complemento]} unless attributes[:complemento].blank?
    @enderecos                                                  = {'CEP' => attributes[:cep]} unless attributes[:cep].blank?
    @fones                                                      = {'CELULAR' => attributes[:celular]} unless attributes[:celular].blank?
    @fones                                                      = {'TELEFONE' => attributes[:telefone]} unless attributes[:telefone].blank?
    @datas                                                      = {'DATA_NASCIMENTO' => attributes[:data_nascimento]} unless attributes[:data_nascimento].blank?
    @rendas                                                     = {'RENDA' => attributes[:renda]} unless attributes[:renda].blank?
    @rendas                                                     = {'RENDA_FAMILIAR' => attributes[:renda_familiar]} unless attributes[:renda_familiar].blank?
    @documentos                                                 = {'CPF' => attributes[:cpf]} unless attributes[:cpf].blank?
    @documentos                                                 = {'CNPJ' => attributes[:cnpj]} unless attributes[:cnpj].blank?
    @documentos                                                 = {'RG' => attributes[:rg]} unless attributes[:rg].blank?
    @documentos                                                 = {'CRECI' => attributes[:creci]} unless attributes[:creci].blank?
    @documentos                                                 = {'CREA' => attributes[:crea]} unless attributes[:crea].blank?
    @documentos                                                 = {'INSCRICAO_ESTADUAL' => attributes[:inscricao_estadual]} unless attributes[:inscricao_estadual].blank?
    @documentos                                                 = {'INSCRICAO_MUNICIPAL' => attributes[:inscricao_municipal]} unless attributes[:inscricao_municipal].blank?
    @origens                                                    = {'NACIONALIDADE' => attributes[:nacionalidade]} unless attributes[:nacionalidade].blank?
    @origens                                                    = {'NATURALIDADE' => attributes[:naturalidade]} unless attributes[:naturalidade].blank?
    @fotos                                                      = {'PERFIL' => attributes[:perfil]} unless attributes[:perfil].blank?
    @fotos                                                      = {'CAPA' => attributes[:capa]} unless attributes[:capa].blank?
    @tipos                                                      = {'SUPERADMIN' => attributes[:superadmin]} unless attributes[:superadmin].blank?
    @tipos                                                      = {'ADMIN' => attributes[:admin]} unless attributes[:admin].blank?
    @tipos                                                      = {'CORRETOR' => attributes[:corretor]} unless attributes[:corretor].blank?
    @tipos                                                      = {'PROPRIETARIO' => attributes[:proprietario]} unless attributes[:proprietario].blank?
    @conjuges                                                   = {'ESTADO_CIVIL' => attributes[:estado_civil]} unless attributes[:estado_civil].blank?
    @conjuges                                                   = {'NOME' => attributes[:conjuge_nome]} unless attributes[:conjuge_nome].blank?
    @conjuges                                                   = {'EMAIL' => attributes[:conjuge_email]} unless attributes[:conjuge_email].blank?
    @conjuges                                                   = {'FONE' => attributes[:conjuge_fone]} unless attributes[:conjuge_fone].blank?
    @conjuges                                                   = {'CPF' => attributes[:conjuge_cpf]} unless attributes[:conjuge_cpf].blank?
    @conjuges                                                   = {'RG' => attributes[:conjuge_rg]} unless attributes[:conjuge_rg].blank?

    unless attributes.blank?
      attributes.merge!(:nomes_fisicos                                              => {'NOME' => attributes[:nome], 'SOBRENOME' => attributes[:sobrenome]}  )
      attributes.merge!(:dados_juridicos                                            => {'NOME_FANTASIA' => attributes[:nome_fantasia], 'RAZAO_SOCIAL' => attributes[:razao_social]}  )
      attributes.merge!(:enderecos                                                  => {'PAIS' => attributes[:pais], 'ESTADO' => attributes[:estado], 'CIDADE' => attributes[:cidade], 'BAIRRO' => attributes[:bairro], 'RUA' => attributes[:rua], 'NUMERO' => attributes[:numero], 'COMPLEMENTO' => attributes[:complemento], 'CEP' => attributes[:cep] } )
      attributes.merge!(:fones                                                      => {'CELULAR' => attributes[:celular], 'TELEFONE' => attributes[:telefone]})
      attributes.merge!(:datas                                                      => {'DATA_NASCIMENTO' => attributes[:data_nascimento]})
      attributes.merge!(:rendas                                                     => {'RENDA' => attributes[:renda], 'RENDA_FAMILIAR' => attributes[:renda_familiar]})
      attributes.merge!(:documentos                                                 => {'CPF' => attributes[:cpf], 'CNPJ' => attributes[:cnpj], 'RG' => attributes[:rg], 'CRECI' => attributes[:creci], 'CREA' => attributes[:crea], 'INSCRICAO_ESTADUAL' => attributes[:inscricao_estadual], 'INSCRICAO_MUNICIPAL' => attributes[:inscricao_municipal] })
      attributes.merge!(:origens                                                    => {'NACIONALIDADE' => attributes[:nacionalidade], 'NATURALIDADE' => attributes[:naturalidade]})
      attributes.merge!(:fotos                                                      => {'PERFIL' => attributes[:perfil], 'CAPA' => attributes[:capa]})
      attributes.merge!(:tipos                                                      => {'SUPERADMIN' => attributes[:superadmin], 'ADMIN' => attributes[:admin], 'CORRETOR' => attributes[:corretor], 'PROPRIETARIO' => attributes[:proprietario] })
      attributes.merge!(:conjuges                                                   => {'ESTADO_CIVIL' => attributes[:estado_civil], 'NOME' => attributes[:conjuge_nome], 'EMAIL' => attributes[:conjuge_email], 'FONE' => attributes[:conjuge_fone], 'CPF' => attributes[:conjuge_cpf], 'RG' => attributes[:conjuge_rg]})
      ((attributes.keys)-(User.column_names.map { |x| x.to_sym })).map { |x| attributes.delete(x.to_sym) unless x.to_s == "password" }
    end

=end