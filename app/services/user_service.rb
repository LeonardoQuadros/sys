require 'rest-client'
require 'json'

class UserService

	def self.set_historico_ip(user, ip)
		begin
			location = Location.geocode(Rails.env.development? ? '179.154.141.205' : ip)
		rescue Exception => e
			Rails.logger.debug("  ################## Exception: #{e} ")
			user.historicos_ips["ERRO"] = e
		end
		user.historicos_ips["#{Date.today}_#{ip}"] =	location
		if user.save
			return "#{Date.today}_#{ip}"
		else
			return nil
		end
	end

	def self.set_endereco(user, cep, params = {})
		begin
			address = Correios::CEP::AddressFinder.get(cep)
		rescue Exception => e
			Rails.logger.debug("  ########## Exception: #{e} ")
			user.enderecos["ERRO"] = "#{e}"
		end
		if user.enderecos["ERRO"].blank?
			user.enderecos['PAIS'] = params["PAIS"].blank? ? "BRASIL" : "#{params['PAIS']}".upcase
			user.enderecos['ESTADO'] = params["ESTADO"].blank? ? "#{address[:state]}".upcase : "#{params['ESTADO']}".upcase
			user.enderecos['CIDADE'] = params["CIDADE"].blank? ? "#{address[:city]}".upcase : "#{params['CIDADE']}".upcase
			user.enderecos['BAIRRO'] = params["BAIRRO"].blank? ? "#{address[:neighborhood]}".upcase : "#{params['BAIRRO']}".upcase
			user.enderecos['RUA'] = params["RUA"].blank? ? "#{address[:address]}".upcase : "#{params['RUA']}".upcase
			user.enderecos['NUMERO'] = params["NUMERO"].blank? ? "--" : "#{params['NUMERO']}".upcase
			user.enderecos['COMPLEMENTO'] = params["COMPLEMENTO"].blank? ? "#{address[:complement]}".upcase : "#{params['COMPLEMENTO']}".upcase
			user.enderecos['CEP'] = params["CEP"].blank? ? "#{address[:zipcode]}".upcase : "#{params['CEP']}".upcase
			user.enderecos["COMPLETO"] = "#{user.enderecos['RUA']} - #{user.enderecos['BAIRRO']} - #{user.enderecos['CIDADE']} - #{user.enderecos['ESTADO']} - #{user.enderecos['CEP']}".upcase
			user.enderecos['JSON'] = {:pais => user.enderecos['PAIS'], :estado => user.enderecos['ESTADO'], :cidade => user.enderecos['CIDADE'], :bairro => user.enderecos['BAIRRO'], :rua => user.enderecos['RUA'], :numero => user.enderecos['NUMERO'], :complemento => user.enderecos['COMPLEMENTO'], :cep => user.enderecos['CEP'], :completo => user.enderecos['COMPLETO']}
		end
		if user.save
			return user.enderecos["ERRO"].blank? ? user.enderecos['JSON'] : user.enderecos["ERRO"]
		else
			return nil
		end
	end

	def self.set_dados_juridicos(user, cnpj, params = {})
    res = RestClient.get "https://www.receitaws.com.br/v1/cnpj/#{cnpj}", :params => {Format: :json}
    json = JSON.parse(res.body)

    json.each do |key, value|
			user.dados_juridicos["#{key}".upcase] =	value
    end

    if user.save
    	return user.dados_juridicos
    else
    	return nil
    end

	end



end



#return "#{address[:address]}, #{address[:neighborhood]}, #{address[:city]} - #{address[:state]} - #{address[:zipcode]}".upcase