class DocumentoUploader < CarrierWave::Uploader::Base
  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def remove_documento!
    begin
      super
    rescue Fog::Storage::Rackspace::NotFound
    end
  end

  # Override to silently ignore trying to remove missing
  # previous documento when saving a new one.
  def remove_previously_stored_documento
    begin
      super
    rescue Fog::Storage::Rackspace::NotFound
      @previous_model_for_documento = nil
    end
  end

end