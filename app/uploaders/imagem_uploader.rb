class ImagemUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick
    include CarrierWave::MiniMagick


  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Local onde será guardado as imagens
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  ## Tamanhos que o CarrierWave vai salvar as imagens

  version :standard do
    process :resize_to_fill => [100, 100]
  end

  version :fill_medium do
    process :resize_to_fill => [300, 300]
  end

  version :fill_big do
    process :resize_to_fill => [600, 600]
  end

  # Thumb
  version :thumb do
     process resize_to_fit: [50, 50]
  end

  # small
  version :small do
     process resize_to_fit: [150, 150]
  end

  # medium
  version :medium do
     process resize_to_fit: [300, 300]
  end

  # Big
  version :big do
     process resize_to_fit: [600, 600]
  end

  # Tipos de extensão aceitas
  def extension_whitelist
     %w(jpg jpeg gif png)
  end
end