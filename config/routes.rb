Rails.application.routes.draw do

  ########################################################################################################
  
  namespace :dashboard do
    
    controller :cursos do
      get '/cursos/gratis' => :gratis, as: :cursos_gratis
      post '/cursos/adquirir/:id' => :adquirir, as: :cursos_adquirir
    end
    resources :cursos, only: [:show, :index]
    
    controller :modulos do
    end
    resources :modulos, only: [:show]
    
    controller :provas do
    end
    resources :provas, only: [:show]
    
    controller :turmas do
      post '/turmas/inscrever_aluno/:id' => :inscrever_aluno, as: :turmas_inscrever_aluno
    end
    resources :turmas, only: [:show]
    
    controller :users do
    end
    resources :users 
    
    controller :pages do
      get '/home' => :home
      get '/perfil' => :perfil
    end

    root 'pages#home'
    get '*path' => redirect('/')
  end

  ########################################################################################################
  
  namespace :admin do
    
    controller :pages do
      get '/home' => :home
      get '/logs' => :logs
    end
    
    controller :categoria_cursos do
    end
    resources :categoria_cursos
    
    controller :cursos do
    end
    resources :cursos
    
    controller :modulos do
    end
    resources :modulos
    
    controller :provas do
    end
    resources :provas
    
    controller :turmas do
    end
    resources :turmas
    
    controller :users do
    end
    resources :users

    root 'pages#home'
    get '*path' => redirect('/')
  end

  ########################################################################################################
  
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: "register"}

  authenticated :user do
    root 'dashboard/pages#home', as: :authenticated_root
  end

  controller :users do
  end
  resources :users

  controller :pages do
    get '/home' => :home
    get '/login' => :login
  end

  root 'pages#home'
  get '*path' => redirect('/')
  ########################################################################################################
end
