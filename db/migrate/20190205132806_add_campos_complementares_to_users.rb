class AddCamposComplementaresToUsers < ActiveRecord::Migration[5.2]
  def change
  	add_column            :users, :token, :string
    add_column 						:users, :nomes_fisicos, :json, default: {} #nome, sobrenome
  	add_column 						:users, :dados_juridicos, :json, default: {} #nome_fantasia, razão_social
  	add_column 						:users, :enderecos, :json, default: {} #rua, bairro, cidade, estado etc
  	add_column 						:users, :conjuges, :json, default: {} #nome, sobrenome, cpf, rg, estado_civil
    add_column            :users, :fones, :json, default: {} #celular, telefone, fax etc
  	add_column 						:users, :datas, :json, default: {} #celular, telefone, fax etc
  	add_column 						:users, :documentos, :json, default: {} #rg, cpf, cnpj, creci, crea etc
  	add_column 						:users, :rendas, :json, default: {} #renda, renda_familiar 
  	add_column 						:users, :origens, :json, default: {} #naturalidade, nacionalidade
  	add_column 						:users, :profissoes, :json, default: {} #estudante, empresário, engenheiro, professor etc
  	add_column 						:users, :fotos, :json, default: {} #perfil, capa etc
  	add_column 						:users, :permissoes, :json, default: {} #usuario_visualiar, usuario_editar, usuario_'action' ... etc
    add_column            :users, :historicos_ips, :json, default: {} # self.historicos_ips["IP"] = {}

    #muda com o tipo de sistema
  	add_column 						:users, :tipos, :json, default: {"ALUNO"=>true, "PROFESSOR"=>false, "ADMIN"=>false, "SUPERADMIN"=>false} # corretor, proprietário, admin, proponente etc
    add_column            :users, :cursos, :json, default: {} # cursos que pode visualizar
  end
end
