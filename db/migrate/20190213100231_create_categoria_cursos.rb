class CreateCategoriaCursos < ActiveRecord::Migration[5.2]
  def change
    create_table :categoria_cursos do |t|
    	t.string				:nome
    	t.string				:descricao
    	t.boolean				:ativo, default: true
    	t.json 					:permitidos, default: {}
    	t.timestamps
    end
  end
end
