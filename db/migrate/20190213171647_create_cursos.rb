class CreateCursos < ActiveRecord::Migration[5.2]
  def change
    create_table :cursos do |t|
    	######################################################################
    	t.string						:nome
    	t.string						:descricao
    	t.string						:imagem_capa #link para url da imagem na raiz do projeto ou externamente
    	t.text							:ementa
    	
    	t.boolean						:gratis, default: true
    	t.boolean						:ativo, default: false
    	t.boolean						:destaque, default: false # mostrar no front

    	#usuário responsável pelo curso (professor)
    	t.references 		    		:responsavel, index: true
    	t.references 		    		:categoria, index: true

    	#preços =>> self.valores["PRECO"]
    	t.json 							:valores, default: {"PRECO"=>0.0, "DESCONTO"=>0.0, "CUSTO"=>0.0, "TOTAL"=>0.0}
    	t.json 							:permitidos, default: {} #usuários permitidos
    	t.json 							:historico, default: {} # historico de alterações



    	######################################################################
    	t.timestamps
    end
  end
end
