class CreateLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :logs do |t|
    	t.string							:descricao
    	t.json 								:comentarios, default: {}
    	t.json 								:objeto, default: {}
    	t.datetime						:viewed_at

    	t.timestamps
    end
  end
end
