class CreateModulos < ActiveRecord::Migration[5.2]
  def change
    create_table :modulos do |t|

    	t.string																:nome
        t.string                                                                :descricao
    	t.integer																:ordem
        
        #tempo máximo para finalizar o módulo
        t.string                                                                :tempo 
    	
        t.string																:video
        t.boolean																:ativo
        
        #conteudo teórico do módulo
        t.text                                                                  :conteudo 
        
        
        #exercícios, slides materias complementares
        t.json                                                                  :materiais 
        
        #array com nome do curso, id e foto pra não precisar carregar o objeto curso
        t.json                                                                  :curso_info, default: {}
        
        #feedback com número de iniciados, comentários, etc
        t.json                                                                  :feedback, default: {"iniciados" => 0, "comentarios" => {}}

    	############################################################################

    	t.references														:curso, index: true

    	############################################################################
    	t.timestamps
    end
  end
end
