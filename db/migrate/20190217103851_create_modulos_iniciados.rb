class CreateModulosIniciados < ActiveRecord::Migration[5.2]
  def change
    create_table :modulos_iniciados do |t|
    	t.string 																:nota

    	t.datetime																:inicio
    	t.datetime																:fim
    	t.json 																	:logs, default: {}
    	t.json 																	:curso_info, default: {}
    	################################################################
    	t.references 															:user, index: true
    	t.references 															:modulo, index: true

    	################################################################
    	t.timestamps
    end
  end
end
