class CreateProvas < ActiveRecord::Migration[5.2]
  def change
    create_table :provas do |t|
    	
    	##########################################################################################
    	t.string													:descricao
    	t.string													:tempo
    	t.boolean													:ativo, default: false

    	##########################################################################################
        t.json                                                      :logs, default: {}
    	t.json 														:perguntas, default: {}

    	t.references 											    :responsavel, index: true
    	t.references 											    :modulo, index: true





    	##########################################################################################
    	t.timestamps
    end
end
end
