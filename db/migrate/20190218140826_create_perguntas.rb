class CreatePerguntas < ActiveRecord::Migration[5.2]
  def change
    create_table :perguntas do |t|
    	t.text									:frase
    	t.integer								:tipos
    	t.integer								:ordem
    	t.references 						:prova, index: true
    end
  end
end
