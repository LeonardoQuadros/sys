class CreateRespostas < ActiveRecord::Migration[5.2]
  def change
    create_table :respostas do |t|
    	t.text									:frase
    	t.boolean								:correta, default: false
    	t.integer 							:ordem
    	t.references 						:pergunta, index: true
    end
  end
end
