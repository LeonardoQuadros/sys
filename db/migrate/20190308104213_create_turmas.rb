class CreateTurmas < ActiveRecord::Migration[5.2]
  def change
    create_table :turmas do |t|
    	
    	t.string 											:inicio
    	t.string 											:fim
    	t.string 											:inicio_inscricao
    	t.string 											:fim_inscricao

        t.string                                            :descricao
    	t.string 											:sigla
    	t.text 												:ementa
    	t.text 												:livros
    	t.text 												:observacoes
    	t.string 											:limite_inscritos

    	t.boolean											:ativo, default: false
    	t.json 												:logs, default: {}
    	t.json 												:alunos, default: {}
    	################################################################################

    	t.references 									:curso, index: true
    	t.references 									:professor, index: true

    	################################################################################
    	t.timestamps
    end
    add_column :cursos, :vitalicio, :boolean, default: false
  end
end
