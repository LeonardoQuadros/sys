class CreateMatriculas < ActiveRecord::Migration[5.2]
  def change
    create_table :matriculas do |t|

    	t.decimal														:nota

    	t.json 															:logs, default: {}

    	####################################################################################

    	t.references 												:aluno, index: true
    	t.references 												:turma, index: true


    	####################################################################################
    	t.timestamps
    end
  end
end
