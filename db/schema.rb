# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_09_172046) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categoria_cursos", force: :cascade do |t|
    t.string "nome"
    t.string "descricao"
    t.boolean "ativo", default: true
    t.json "permitidos", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cursos", force: :cascade do |t|
    t.string "nome"
    t.string "descricao"
    t.string "imagem_capa"
    t.text "ementa"
    t.boolean "gratis", default: true
    t.boolean "ativo", default: false
    t.boolean "destaque", default: false
    t.bigint "responsavel_id"
    t.bigint "categoria_id"
    t.json "valores", default: {"PRECO"=>0.0, "DESCONTO"=>0.0, "CUSTO"=>0.0, "TOTAL"=>0.0}
    t.json "permitidos", default: {}
    t.json "historico", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "vitalicio", default: false
    t.index ["categoria_id"], name: "index_cursos_on_categoria_id"
    t.index ["responsavel_id"], name: "index_cursos_on_responsavel_id"
  end

  create_table "logs", force: :cascade do |t|
    t.string "descricao"
    t.json "comentarios", default: {}
    t.json "objeto", default: {}
    t.datetime "viewed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "matriculas", force: :cascade do |t|
    t.decimal "nota"
    t.json "logs", default: {}
    t.bigint "aluno_id"
    t.bigint "turma_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["aluno_id"], name: "index_matriculas_on_aluno_id"
    t.index ["turma_id"], name: "index_matriculas_on_turma_id"
  end

  create_table "modulos", force: :cascade do |t|
    t.string "nome"
    t.string "descricao"
    t.integer "ordem"
    t.string "tempo"
    t.string "video"
    t.boolean "ativo"
    t.text "conteudo"
    t.json "materiais"
    t.json "curso_info", default: {}
    t.json "feedback", default: {"iniciados"=>0, "comentarios"=>{}}
    t.bigint "curso_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["curso_id"], name: "index_modulos_on_curso_id"
  end

  create_table "modulos_iniciados", force: :cascade do |t|
    t.string "nota"
    t.datetime "inicio"
    t.datetime "fim"
    t.json "logs", default: {}
    t.json "curso_info", default: {}
    t.bigint "user_id"
    t.bigint "modulo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["modulo_id"], name: "index_modulos_iniciados_on_modulo_id"
    t.index ["user_id"], name: "index_modulos_iniciados_on_user_id"
  end

  create_table "perguntas", force: :cascade do |t|
    t.text "frase"
    t.integer "tipos"
    t.integer "ordem"
    t.bigint "prova_id"
    t.index ["prova_id"], name: "index_perguntas_on_prova_id"
  end

  create_table "provas", force: :cascade do |t|
    t.string "descricao"
    t.string "tempo"
    t.boolean "ativo", default: false
    t.json "logs", default: {}
    t.json "perguntas", default: {}
    t.bigint "responsavel_id"
    t.bigint "modulo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["modulo_id"], name: "index_provas_on_modulo_id"
    t.index ["responsavel_id"], name: "index_provas_on_responsavel_id"
  end

  create_table "respostas", force: :cascade do |t|
    t.text "frase"
    t.boolean "correta", default: false
    t.integer "ordem"
    t.bigint "pergunta_id"
    t.index ["pergunta_id"], name: "index_respostas_on_pergunta_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "var", null: false
    t.text "value"
    t.integer "thing_id"
    t.string "thing_type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true
  end

  create_table "turmas", force: :cascade do |t|
    t.string "inicio"
    t.string "fim"
    t.string "inicio_inscricao"
    t.string "fim_inscricao"
    t.string "descricao"
    t.string "sigla"
    t.text "ementa"
    t.text "livros"
    t.text "observacoes"
    t.string "limite_inscritos"
    t.boolean "ativo", default: false
    t.json "logs", default: {}
    t.json "alunos", default: {}
    t.bigint "curso_id"
    t.bigint "professor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["curso_id"], name: "index_turmas_on_curso_id"
    t.index ["professor_id"], name: "index_turmas_on_professor_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "token"
    t.json "nomes_fisicos", default: {}
    t.json "dados_juridicos", default: {}
    t.json "enderecos", default: {}
    t.json "conjuges", default: {}
    t.json "fones", default: {}
    t.json "datas", default: {}
    t.json "documentos", default: {}
    t.json "rendas", default: {}
    t.json "origens", default: {}
    t.json "profissoes", default: {}
    t.json "fotos", default: {}
    t.json "permissoes", default: {}
    t.json "historicos_ips", default: {}
    t.json "tipos", default: {"ALUNO"=>true, "PROFESSOR"=>false, "ADMIN"=>false, "SUPERADMIN"=>false}
    t.json "cursos", default: {}
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
