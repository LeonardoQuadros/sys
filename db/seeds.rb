
Faker::Config.locale = 'pt-BR'
Log.destroy_all
User.destroy_all
user = User.create(nome: "Sys", email: "sys@sys.com.br", password: "sys", superadmin: true)
aluno = User.create(nome: "Aluno", email: "aluno@sys.com.br", password: "sys", aluno: true)
professor = User.create(nome: "Professor", email: "professor@sys.com.br", password: "sys", professor: true)

CategoriaCurso.all.destroy_all
(1..10).each do |index|
	CategoriaCurso.create(nome: Faker::Educator.subject, descricao: Faker::Lorem.paragraph, ativo: [true, false].sample)
end

(1..30).each do |index|
	preco = [0, 0, 100, 200, 300].sample
	desconto = [0, 0, 0, 10, 20, 30].sample
	total = preco-(preco*((desconto/100)))
	Curso.create(nome: Faker::Educator.subject, descricao: Faker::Lorem.paragraph, ementa: Faker::Lorem.paragraph(50), destaque: [true, false].sample, responsavel_id: User.ids.sample, categoria_id: CategoriaCurso.ids.sample, preco: preco, custo: [0, 0, 0, 100, 200, 300].sample, total: total, desconto: desconto, gratis: (total==0), vitalicio: [true, false].sample)
end

Curso.all.each do |curso|
	(1..5).each do |index|
		modulo = curso.modulos.order("ordem asc").last
		Modulo.create(nome: Faker::Job.position, descricao: Faker::Lorem.paragraph, video: "https://www.youtube.com/embed/wJnBTPUQS5A", ativo: [true, false].sample, curso_id: curso.id, conteudo: Faker::Lorem.paragraph(50), tempo: [0,0,0,1,2,3].sample, ordem: (modulo.blank? ? 1 : modulo.ordem+1) )
	end
	curso.update(ativo: true)
end


Modulo.all.each do |modulo|
	prova = Prova.create(descricao: "Avaliação de #{Faker::Educator.subject}", responsavel_id: user.id, modulo_id: modulo.id)
	(1..5).each do |index|
		tipo = [0, 1].sample
		pergunta = Pergunta.create(frase: Faker::Lorem.paragraph, prova_id: prova.id, tipos: tipo)
		if pergunta.alternativa?
			(1..4).each do |index|
				Resposta.create(frase: Faker::Lorem.paragraph, correta: [true, false].sample, pergunta_id: pergunta.id)
			end
		end
	end
	prova.update(ativo: true)
end

Curso.where(vitalicio: false).each do |curso|
	Turma.create(inicio: "01/02/2019", fim: "01/07/2019", inicio_inscricao: "01/02/2019", fim_inscricao: "01/04/2019", 
								descricao: "#{curso.nome} - Turma 02/2019", ementa: Faker::Lorem::paragraph(50), limite_inscritos: 40,
								ativo: true, curso_id: curso.id, professor_id: professor.id)
end

(1..100).each do |aluno|
	user = User.create(nome: Faker::Name.name_with_middle, email: Faker::Internet.safe_email, password: Faker::Internet.password, aluno: true)
	Matricula.create(turma_id: Turma.ids.sample, aluno_id: user.id)
end

