require 'rails_helper'

RSpec.describe UsersController, type: :controller do

		context "criar usuário" do
			it "quando parâmetros são válidos" do 
	      expect{
	        post :create, params: {user: {email: "leooq.d@gmail.com", nome: "Leonardo Quadros", password: "mudarsenha@123"}}
	      }.to change(User, :count).by(1) 
			end
			# teste para saber se está salvando usuários inválidos
	    it "quando parâmetros não são válidos" do
	      expect{
	        post :create, params: {user: FactoryBot.attributes_for(:invalid_user)}
	      }.to_not change(User,:count)
	    end
		end

end