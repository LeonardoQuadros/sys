require 'faker'

FactoryBot.define do
	Faker::Config.locale = 'pt-BR'
	factory :curso do
		nome { "#{Faker::Educator.subject}__" }
		categoria_id { CategoriaCurso.ids.sample }
		responsavel_id { User.ids.sample }
	end

end
