require 'faker'

FactoryBot.define do
	Faker::Config.locale = 'pt-BR'
	factory :user do
		nomes_fisicos { {"NOME" => Faker::Name.first_name, "SOBRENOME" => "#{Faker::Name.last_name}"} }
		email { Faker::Internet.safe_email }
		password { Faker::Internet.password } 
	end

	factory :full_user, parent: :user do
		nomes_fisicos { {"NOME" => "JOAO88", "SOBRENOME" => "#{Faker::Name.last_name}"} }
		dados_juridicos { {"RAZAO_SOCIAL" => "#{Faker::Company.industry} #{Faker::Company.suffix}", "NOME_FANTASIA" => "#{Faker::Company.name}"} }
		documentos { {"CPF" => "000.000.000-00", "CNPJ" => "95.545.434/0001-83", "RG" => "111111", "CRECI" => "111111", "CREA" => "111111", "INSCRICAO_ESTADUAL" => "111111", "INSCRICAO_MUNICIPAL" => "111111"} }
		fones { {"CELULAR" => Faker::PhoneNumber.cell_phone, "TELEFONE" => Faker::PhoneNumber.phone_number} }
		datas { {"DATA_NASCIMENTO" => "02/09/1994"}}
		rendas { {"RENDA" => "30.000,00", "RENDA_FAMILIAR" => "30.000,00"}}
		origens { {"NACIONALIDADE" => Faker::Address.country, "NATURALIDADE" => Faker::Address.city} }
		enderecos { {"PAIS" => Faker::Address.country, "ESTADO" => Faker::Address.state, "CIDADE" => Faker::Address.city,
			"BAIRRO" => Faker::Address.community, "RUA" => Faker::Address.street_name, "NUMERO" => Faker::Address.building_number, 
			"COMPLEMENTO" => Faker::Address.secondary_address, "CEP" => Faker::Address.zip_code} }
			profissoes { {"ENGENHEIRO" => "5 anos"}}
			tipos { {"SUPERADMIN" => [true, false].sample, "ADMIN" => [true, false].sample, "CORRETOR" => [true, false].sample, "PROPRIETARIO" => [true, false].sample }}
			conjuges { {"NOME" => Faker::Name.first_name, "EMAIL" => "#{Faker::Internet.safe_email}", "CPF" => "000.000.000-00", "RG" => "000.0000", "ESTADO_CIVIL" => ["SOLTEIRO(A)", "CASADO(A)", "DIVORCIADO(A)"].sample} }
			email { Faker::Internet.safe_email }
			password { Faker::Internet.password } 
		end
		factory :invalid_user, parent: :user do
			nomes_fisicos { {"NOME" => nil} }
			email { nil }
			password { nil }
		end
	end
