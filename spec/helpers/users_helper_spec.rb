require 'rails_helper'
 
RSpec.describe UserHelper, type: :helper do
    describe "atalhos" do
      it "nome" do
				@user = create(:user)
        expect(helper.nome(@user)).to eq("#{@user.nomes_fisicos['NOME']}")
      end
      it "sobrenome" do
				@user = create(:user)
        expect(helper.sobrenome(@user)).to eq("#{@user.nomes_fisicos['SOBRENOME']}")
      end
      it "nome_fantasia" do
				@user = create(:user)
        expect(helper.nome_fantasia(@user)).to eq("#{@user.dados_juridicos['NOME_FANTASIA']}")
      end
      it "razao_social" do
				@user = create(:user)
        expect(helper.razao_social(@user)).to eq("#{@user.dados_juridicos['RAZAO_SOCIAL']}")
      end
      it "celular" do
				@user = create(:user)
        expect(helper.celular(@user)).to eq("#{@user.fones['CELULAR']}")
      end
      it "telefone" do
				@user = create(:user)
        expect(helper.telefone(@user)).to eq("#{@user.fones['TELEFONE']}")
      end
      it "data_nascimento" do
				@user = create(:user)
        expect(helper.data_nascimento(@user)).to eq("#{@user.datas['DATA_NASCIMENTO']}")
      end
      it "renda" do
				@user = create(:user)
        expect(helper.renda(@user)).to eq("#{@user.rendas['RENDA']}")
      end
      it "renda_familiar" do
				@user = create(:user)
        expect(helper.renda_familiar(@user)).to eq("#{@user.rendas['RENDA_FAMILIAR']}")
      end
      it "cpf" do
				@user = create(:user)
        expect(helper.cpf(@user)).to eq("#{@user.documentos['CPF']}")
      end
      it "cnpj" do
				@user = create(:user)
        expect(helper.cnpj(@user)).to eq("#{@user.documentos['CNPJ']}")
      end
      it "rg" do
				@user = create(:user)
        expect(helper.rg(@user)).to eq("#{@user.documentos['RG']}")
      end
      it "creci" do
				@user = create(:user)
        expect(helper.creci(@user)).to eq("#{@user.documentos['CRECI']}")
      end
      it "crea" do
				@user = create(:user)
        expect(helper.crea(@user)).to eq("#{@user.documentos['CREA']}")
      end
      it "inscricao_estadual" do
				@user = create(:user)
        expect(helper.inscricao_estadual(@user)).to eq("#{@user.documentos['INSCRICAO_ESTADUAL']}")
      end
      it "inscricao_municipal" do
				@user = create(:user)
        expect(helper.inscricao_municipal(@user)).to eq("#{@user.documentos['INSCRICAO_MUNICIPAL']}")
      end
      it "pais" do
				@user = create(:user)
        expect(helper.pais(@user)).to eq("#{@user.enderecos['PAIS']}")
      end
      it "estado" do
				@user = create(:user)
        expect(helper.estado(@user)).to eq("#{@user.enderecos['ESTADO']}")
      end
      it "cidade" do
				@user = create(:user)
        expect(helper.cidade(@user)).to eq("#{@user.enderecos['CIDADE']}")
      end
      it "bairro" do
				@user = create(:user)
        expect(helper.bairro(@user)).to eq("#{@user.enderecos['BAIRRO']}")
      end
      it "rua" do
				@user = create(:user)
        expect(helper.rua(@user)).to eq("#{@user.enderecos['RUA']}")
      end
      it "numero" do
				@user = create(:user)
        expect(helper.numero(@user)).to eq("#{@user.enderecos['NUMERO']}")
      end
      it "complemento" do
				@user = create(:user)
        expect(helper.complemento(@user)).to eq("#{@user.enderecos['COMPLEMENTO']}")
      end
      it "cep" do
				@user = create(:user)
        expect(helper.cep(@user)).to eq("#{@user.enderecos['CEP']}")
      end
    end
end
