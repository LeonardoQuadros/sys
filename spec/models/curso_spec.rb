require 'rails_helper'

RSpec.describe Curso, type: :model do

	context "criando com initialize" do
		before do
			@curso = create(:curso)
		end

		it "quando parâmetros obrigatórios estão preenchidos" do
			expect(@curso.valid?).to eq(true)
		end

	end
end