require 'rails_helper'

RSpec.describe User, type: :model do

	context "criando com initialize" do
		before do
			@user = create(:user)
		end

		it "quando parâmetros obrigatórios estão preenchidos" do
			expect(@user.valid?).to eq(true)
		end

		it "quando parâmetros obrigatórios não estão preenchidos" do
			expect(@user.update(FactoryBot.attributes_for(:invalid_user))).to eq(false)
		end


	end
end