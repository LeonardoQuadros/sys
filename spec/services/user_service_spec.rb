require 'spec_helper'
require './app/services/user_service'

describe 'set_ip' do
    it 'set_ip' do
    @user = create(:user)
    ip = '179.154.141.205'
    res = UserService.set_historico_ip(@user, ip)

    expect(res.is_a? String).to eql(true)
    expect(@user.historicos_ips["#{Date.today}_#{ip}"].is_a? Hash).to eql(true)
    expect(res == "#{Date.today}_#{ip}").to eql(true)
    end
end

describe 'set_endereco' do
    it 'set_endereco' do
    @user = create(:user)
    res = UserService.set_endereco(@user, '13560049', {"NUMERO" => "2386"})

    expect((!@user.enderecos["ERRO"].blank? or res.is_a? Hash)).to eql(true)
    expect((!@user.enderecos["ERRO"].blank? or @user.enderecos["NUMERO"] == "2386")).to eql(true)
    expect((!@user.enderecos["ERRO"].blank? or @user.enderecos["CIDADE"] == "SÃO CARLOS")).to eql(true)
    end
end


describe 'set_dados_juridicos' do
    it 'set_dados_juridicos' do
    @user = create(:user)
    res = UserService.set_dados_juridicos(@user, '09499016000165')

    #expect(res.is_a? Hash).to eql(true)
    expect(res["FANTASIA"] == "IPLANO").to eql(true)
    end
end
